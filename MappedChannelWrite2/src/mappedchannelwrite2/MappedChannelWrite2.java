/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mappedchannelwrite2;

// Write to a mapped file. Pre JDK 7 version. 
import java.io.*; 
import java.nio.*; 
import java.nio.channels.*; 

/**
 *
 * @author artyuhovyv
 */
public class MappedChannelWrite2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         RandomAccessFile fOut = null; 
    FileChannel fChan = null; 
    ByteBuffer mBuf; 
 
    try { 
      fOut = new RandomAccessFile("test.txt", "rw"); 
 
      // Next, obtain a channel to that file. 
      fChan = fOut.getChannel(); 
 
      // Then, map the file into a buffer. 
      mBuf = fChan.map(FileChannel.MapMode.READ_WRITE, 0, 26); 
 
      // Write some bytes to the buffer. 
      for(int i=0; i<26; i++) 
        mBuf.put((byte)('A' + i)); 
 
    } catch (IOException e) { 
      System.out.println("I/O Error " + e); 
    } finally { 
      try { 
        if(fChan != null) fChan.close(); // close channel 
      } catch(IOException e) { 
        System.out.println("Error Closing Channel."); 
      } 
      try { 
        if(fOut != null) fOut.close(); // close file 
      } catch(IOException e) { 
        System.out.println("Error Closing File."); 
      } 
    } 
 }
    
}
