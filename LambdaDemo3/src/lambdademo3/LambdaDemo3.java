/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdademo3;

interface NumericTest2 { 
  boolean test(int n, int d); 
} 

/**
 *
 * @author artyuhovyv
 */

public class LambdaDemo3 {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
         NumericTest2 isFactor = (n, d) -> (n % d) == 0; 
 
        if(isFactor.test(10, 2)) 
          System.out.println("2 is a factor of 10"); 

        if(!isFactor.test(10, 3)) 
          System.out.println("3 is not a factor of 10");
    }
    
}
