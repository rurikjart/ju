/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package urldemo;

import java.net.*;

/**
 *
 * @author artyuhovyv
 */
public class URLDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException {
        // TODO code application logic here
         URL hp = new URL("http://www.HerbSchildt.com/WhatsNew"); 
 
            System.out.println("Protocol: " + hp.getProtocol()); 
            System.out.println("Port: " + hp.getPort()); 
            System.out.println("Host: " + hp.getHost()); 
            System.out.println("File: " + hp.getFile()); 
            System.out.println("Ext:" + hp.toExternalForm()); 
    }  
}
