/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package curdemo;


// Demonstrate Currency.
import java.util.*;

/**
 *
 * @author artyuhovyv
 */
public class CurDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Currency c;
        
         
          c = Currency.getInstance(Locale.US);

            System.out.println("Symbol: " + c.getSymbol());
            System.out.println("Default fractional digits: " +
                               c.getDefaultFractionDigits());
    
    }
    
}
