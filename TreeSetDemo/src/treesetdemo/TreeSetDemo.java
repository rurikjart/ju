/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package treesetdemo;

/**
 *
 * @author artyuhovyv
 */

// Demonstrate TreeSet. 
import java.util.*; 

public class TreeSetDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         // Create a tree set. 
        TreeSet<String> ts = new TreeSet<String>(); 

        // Add elements to the tree set. 
        ts.add("C"); 
        ts.add("A"); 
        ts.add("B"); 
        ts.add("E"); 
        ts.add("F"); 
        ts.add("D"); 

        System.out.println(ts); 
    }
    
}
