/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scopeerr;

/**
 *
 * @author rurik
 */
public class ScopeErr {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         int bar = 1;
     {              // creates a new scope
       int bar2 = 2; // Compile time error -- bar already defined!
     }
    }
    
}
