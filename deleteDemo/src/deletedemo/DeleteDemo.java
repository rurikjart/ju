/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deletedemo;

/**
 *
 * @author rurik
 */
public class DeleteDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         StringBuffer sb = new StringBuffer("This is a test.");

            sb.delete(4, 7);
            System.out.println("After delete: " + sb);

            sb.deleteCharAt(0);
            System.out.println("After deleteCharAt: " + sb);
    }
    
}
