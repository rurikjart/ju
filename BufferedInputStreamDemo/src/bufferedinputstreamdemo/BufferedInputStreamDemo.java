/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bufferedinputstreamdemo;

// Use buffered input. 
// This program uses try-with-resources. It requires JDK 7 or later. 
 
import java.io.*; 

/**
 *
 * @author artyuhovyv
 */
public class BufferedInputStreamDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
          String s = "This is a &copy; copyright symbol " + 
      "but this is &copy not.\n"; 
    byte buf[] = s.getBytes(); 
 
    ByteArrayInputStream in = new ByteArrayInputStream(buf); 
    int c; 
    boolean marked = false; 
 
    // Use try-with-resources to manage the file. 
    try ( BufferedInputStream f = new BufferedInputStream(in) ) 
    { 
      while ((c = f.read()) != -1) { 
        switch(c) { 
        case '&': 
          if (!marked) { 
            f.mark(32); 
            marked = true; 
          } else { 
            marked = false; 
          } 
          break; 
        case ';': 
          if (marked) { 
            marked = false; 
            System.out.print("(c)"); 
          } else 
            System.out.print((char) c); 
          break; 
        case ' ': 
          if (marked) { 
            marked = false; 
            f.reset(); 
            System.out.print("&"); 
          } else 
            System.out.print((char) c); 
          break; 
        default: 
          if (!marked) 
            System.out.print((char) c); 
          break; 
        } 
      } 
    } catch(IOException e) { 
      System.out.println("I/O Error: " + e); 
    } 
    }
    
}
