/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package htdemo2;

/**
 *
 * @author artyuhovyv
 */

import java.util.*;

public class HTDemo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
          Hashtable<String, Double> balance = 
          new Hashtable<String, Double>(); 
 
            String str; 
            double bal; 

            balance.put("John Doe", 3434.34); 
            balance.put("Tom Smith", 123.22); 
            balance.put("Jane Baker", 1378.00); 
            balance.put("Tod Hall", 99.22); 
            balance.put("Ralph Smith", -19.08); 

            // Show all balances in hashtable. 
            // First, get a set view of the keys. 
            Set<String> set = balance.keySet(); 

            // Get an iterator. 
            Iterator<String> itr = set.iterator(); 
            while(itr.hasNext()) { 
              str = itr.next(); 
              System.out.println(str + ": " + 
                                 balance.get(str)); 
            } 

            System.out.println(); 

            // Deposit 1,000 into John Doe's account. 
            bal = balance.get("John Doe"); 
            balance.put("John Doe", bal+1000); 
            System.out.println("John Doe's new balance: " + 
                               balance.get("John Doe")); 
    }
    
}
