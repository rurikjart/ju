/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genconsdemo;

/**
 *
 * @author artyuhovyv
 */
class GenCons { 
  private double val; 
 
  <T extends Number> GenCons(T arg) { 
    val = arg.doubleValue(); 
  } 
 
  void showval() { 
    System.out.println("val: " + val); 
  } 
} 
 
class GenConsDemo { 
  public static void main(String args[]) { 
 
    GenCons test = new GenCons(100); 
    GenCons test2 = new GenCons(123.5F); 
 
    test.showval(); 
    test2.showval(); 
  } 
}