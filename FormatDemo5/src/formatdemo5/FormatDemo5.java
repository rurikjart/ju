/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formatdemo5;

// Demonstrate the space format specifiers. 
import java.util.*; 

/**
 *
 * @author artyuhovyv
 */
public class FormatDemo5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Formatter fmt = new Formatter(); 
 
            fmt.format("% d", -100); 
            System.out.println(fmt); 
            fmt.close();

            fmt = new Formatter(); 
            fmt.format("% d", 100); 
            System.out.println(fmt); 
            fmt.close();

            fmt = new Formatter(); 
            fmt.format("% d", -200); 
            System.out.println(fmt); 
            fmt.close();

            fmt = new Formatter(); 
            fmt.format("% d", 200); 
            System.out.println(fmt); 
            fmt.close();
    }
    
}
