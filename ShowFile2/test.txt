�Near-term attacks�: State Dept. issues Europe-wide travel 
alert as US ramps up security at home 
Citing risks of �near-term� terrorist attacks, 
the US has warned Americans against traveling 
anywhere in Europe and boosted security at home since 
twin blasts in Brussels left 31 dead and over 250 wounded, 
including several US citizens. 