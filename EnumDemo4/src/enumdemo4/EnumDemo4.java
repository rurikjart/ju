/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumdemo4;

/**
 *
 * @author artyuhovyv
 */

// Use an enum constructor. 
enum Apple { 
  Jonathan(10), GoldenDel(9), RedDel, Winesap(15), Cortland(8); 
 
  private int price; // price of each apple 
 
  // Constructor 
  Apple(int p) { price = p; } 
 
  // Overloaded constructor 
  Apple() { price = -1; } 
 
  int getPrice() { return price; } 
}

public class EnumDemo4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         Apple ap;
        
         // Display price of Winesap. 
            System.out.println("Winesap costs " + 
                               Apple.Winesap.getPrice() + 
                               " cents.\n"); 

            // Display all apples and prices. 
            System.out.println("All apple prices:"); 
            for(Apple a : Apple.values()) 
              System.out.println(a + " costs " + a.getPrice() + 
                                 " cents."); 
        
    }
    
}
