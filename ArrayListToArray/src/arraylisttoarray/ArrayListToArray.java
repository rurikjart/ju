/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraylisttoarray;

/**
 *
 * @author artyuhovyv
 */

// Convert an ArrayList into an array.  
import java.util.*;  

public class ArrayListToArray {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        
      // Create an array list. 
    ArrayList<Integer> al = new ArrayList<Integer>();  
      
    // Add elements to the array list. 
    al.add(1);  
    al.add(2);  
    al.add(3);  
    al.add(4);  
  
    System.out.println("Contents of al: " + al);  
  
    // Get the array. 
    Integer ia[] = new Integer[al.size()];  
    ia = al.toArray(ia);  
  
    int sum = 0;  
  
    // Sum the array. 
    for(int i : ia) sum += i;  
  
    System.out.println("Sum is: " + sum);    
        
        
    }
    
}
