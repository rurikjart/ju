/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Using arraycopy().

package acdemo;

/**
 *
 * @author artyuhovyv
 */
public class ACDemo {

     static byte a[] = { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74 };
     static byte b[] = { 77, 77, 77, 77, 77, 77, 77, 77, 77, 77 };
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
            System.out.println("a = " + new String(a));
            System.out.println("b = " + new String(b));
            System.arraycopy(a, 0, b, 0, a.length);
            System.out.println("a = " + new String(a));
            System.out.println("b = " + new String(b));
            System.arraycopy(a, 0, a, 1, a.length - 1);
            System.arraycopy(b, 1, b, 0, b.length - 1);
            System.out.println("a = " + new String(a));
            System.out.println("b = " + new String(b));
    }
    
}
