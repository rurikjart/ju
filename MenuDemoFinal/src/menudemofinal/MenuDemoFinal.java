/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menudemofinal;

// продемонстрировать меню - окончательный вариант

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.input.*;
import javafx.scene.image.*;
import javafx.beans.value.*;
import jdk.nashorn.internal.ir.CallNode;


/**
 *
 * @author artyuhovyv
 */

public class MenuDemoFinal extends Application {

    MenuBar mb;
    EventHandler<ActionEvent> MEHandler;
    ContextMenu editMenu;
    ToolBar tbDebug;
    
    Label response;
    
    public static void main(String[] args) {
       // запустить JavaFX-приложение, вызвав метод Launch()
        launch(args);
    }
    
    //переопределить метод start()
    @Override
    public void start(Stage myStage) {
        
        //Присвоить заголовок подмосткам
        myStage.setTitle("Demonstrate Menus -- Final Version");
        //Демонстрация меню - окончательный вариант
        
        //Использовать панель граничной компоновки BorderPane
        //в качестве корневого узла
        final BorderPane rootNode = new BorderPane();
        
        //создать сцену
        Scene myScene = new Scene(rootNode,300,300);
        
        //установить сцену на подмостках
        myStage.setScene(myScene);
        
        //зоздать метку для отображения результатов выбора
        //из разных элементов управления ГПИ приложения
        response = new Label();
        
        //создать один приемник действий для обработки
        //всех событий действий, наступающих в меню
        
        MEHandler = new EventHandler<ActionEvent>() { 
      public void handle(ActionEvent ae) {  
        String name = ((MenuItem)ae.getTarget()).getText(); 
 
        if(name.equals("Exit")) Platform.exit(); 
 
        response.setText( name + " selected"); 
         }  
       }; 
        
        //создать строку меню
        mb = new MenuBar();
        
        //создать меню File
        makeFileMenu();
        
        //создать меню Options
        makeOptionsMenu();
        
        //создать меню Rurik Wrestling
        makeWrestlingMenu();
        
        //создать меню Help
        makeHelpMenu();
        
        //создать контекстное меню
        makeContextMenu();
        
        //создать текстовое поле, зададав ширину его столбца равной 20
        TextField tf = new TextField();
        tf.setPrefColumnCount(20);
        
        //ввести контекстное меню в текстовое поле
        tf.setContextMenu(editMenu);
        
        //создать панель инструментов
        makeToolBar();
        
        //ввести контекстное меню непосредственно
        //в граф сцены
        rootNode.setOnContextMenuRequested( 
                new EventHandler<ContextMenuEvent>() {   
      public void handle(ContextMenuEvent ae) {  
        // Popup menu at the location of the right click. 
        editMenu.show(rootNode, ae.getScreenX(), ae.getScreenY()); 
          }  
        });  
        
        
        //вывести строку меню в верхней области панели
        rootNode.setTop(mb);
        
        //создать панель поточной компоновки для хранения
        //текстового поля и метки ответной реакции на 
        //действия пользователя
        FlowPane fpRoot = new FlowPane(10,10);
        
        //выровнять элементы управления по центру сцены
        fpRoot.setAlignment(Pos.CENTER);
        
        //использовать разделитель, чтобы улучшить порядок
        //расположения элементов управления
        Separator separator = new Separator();
        separator.setPrefWidth(260);
        
        //ввести метку, разделитель и текстовое поле на
        //панели поточной компоновки
        fpRoot.getChildren().addAll(response, separator, tf);
        
        //ввести панель инструментов в нижней области панели
        //граничной компоновки
        rootNode.setBottom(tbDebug);
        
        //ввести панель поточной компоновки в центральной области
        //панели граничной компоновки
        
        rootNode.setCenter(fpRoot);
        
        //показать подмостки и сцену на них
        myStage.show();
    }
    
    //создать меню File с мнемоникой
    void makeFileMenu(){
        Menu fileMenu = new Menu("_File");//Файл
        
        //создать отдельные пункты меню File
        MenuItem open = new MenuItem("Open");//Открыть
        MenuItem close = new MenuItem("Close");//Закрыть
        MenuItem save = new MenuItem("Save");//Сохранить
        MenuItem exit = new MenuItem("Exit");//Выход
        
        //вывести пункты в меню File
        fileMenu.getItems().addAll(open, close, save, new SeparatorMenuItem(), exit);
        
        // вывести оперативные клавиши для быстрого выбора
        // пунктов из меню File
        open.setAccelerator(KeyCombination.keyCombination("shortcut+O"));
        close.setAccelerator(KeyCombination.keyCombination("shortcut+C"));
        save.setAccelerator(KeyCombination.keyCombination("shortcut+S"));
        exit.setAccelerator(KeyCombination.keyCombination("shortcut+E"));
        
        //установить обработчики событий действия для пунктов меню File
        open.setOnAction(MEHandler);
        close.setOnAction(MEHandler);
        save.setOnAction(MEHandler);
        exit.setOnAction(MEHandler);
        
        //ввести меню File в строку меню
        mb.getMenus().add(fileMenu);
    }

    //сщздать меню Options
     void makeOptionsMenu() {
       
         
         Menu optionsMenu = new Menu("Options");//Параметры
         
         //создать подменю Colors
         Menu colorsMenu = new Menu("Colors");//Цвета
      
         
         //использовать отмечаемые флажками пункты меню,
         // чтобы пользователь мог выбрать сразу несколько цветов
         CheckMenuItem red = new CheckMenuItem("red"); //Красный
         CheckMenuItem green = new CheckMenuItem("Green");//Зеленый
         CheckMenuItem blue = new CheckMenuItem("Blue");//Синий
         
         //ввестиотмечаемые флажками пункты в подменю Colors,
         // а само подменю Colors - в меню Options
         
         colorsMenu.getItems().addAll(red,green,blue);
         optionsMenu.getItems().add(colorsMenu);
         
         //задать зеленый цвет в качестве исходного выбираемого
         green.setSelected(true);
         
          //создать подменю Priority
         Menu priorityMenu = new Menu("Priority");//Приоритет
         
         //использовать отмечаемые кнопками-переключателями пункты
         //меню для установки приоритета. Благодаря этому в меню не
         //только отображается установленный приоритет, но и 
         //гарантируется установка одного и только одного приоритета
         RadioMenuItem high = new RadioMenuItem("High");
         RadioMenuItem low = new RadioMenuItem("Low");
         
         //Создать группу кнопок-переключателей в пунктах подменю Priority
         ToggleGroup tg = new ToggleGroup();
         high.setToggleGroup(tg);
         low.setToggleGroup(tg);
         
         //отметить пункт меню High как исходно выбираемый
         high.setSelected(true);
         
         //ввести отмечаемые кнопками-переключателями пункты
         //в подменю Priority, последнее - в меню Options
         priorityMenu.getItems().addAll(high,low);
         optionsMenu.getItems().add(priorityMenu);
         
         //ввести разделитель
         optionsMenu.getItems().add(new SeparatorMenuItem());
         
         //создать пункт меню Reset и ввести его в меню Options
         MenuItem reset = new MenuItem("Reset"); //Сбросить
         optionsMenu.getItems().add(reset);
         
         //установить обработчики событий действия для
         //пунктов меню Options
         red.setOnAction(MEHandler);
         green.setOnAction(MEHandler);
         blue.setOnAction(MEHandler);
         high.setOnAction(MEHandler);
         low.setOnAction(MEHandler);
         reset.setOnAction(MEHandler);
         
         //использовать приемник событий изменения, чтобы оперативно
         //реагировать на изменения в отметке пунктов подменю Priority
         //кнопками-переключателями
         tg.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
         public void changed(ObservableValue<? extends Toggle> changed, Toggle oldVal,Toggle newVal){
         if(newVal==null) return;
         
         //привести значение newVal к типу RadioButton
         RadioMenuItem rmi = (RadioMenuItem) newVal;
         
         //отобразить результат выбора приоритета
         response.setText("Prioruty selected is " + rmi.getText());
         //выбран указанный приоритет
         
         }
         });
         
          // вывести меню Оptions в строку меню
          mb.getMenus().add(optionsMenu);
         
    }

     //создать борцовкое меню
     void makeWrestlingMenu(){
         
         //создать меню rurik_wrestling
         Menu menuWrestling = new Menu("Rurik Wrestling");
         
         //создать пункт greco-roman wrestling
         Menu grw = new Menu("Greco-roman wrestling");
         
         //создать пункт free style wrestling
         Menu fsw = new Menu("Free Style Wrestling");
         
         //создать пункт Grappling
         MenuItem gw = new MenuItem("Grappling Wrestling");
         
         
         //добавить все пункты в меню Rurik Wrestling
         menuWrestling.getItems().addAll(grw,fsw,gw);
         
          MenuItem reds = new MenuItem("red singlet"); //Красный
          MenuItem blues = new MenuItem("blue singlet");//Синий
          
          MenuItem redf = new MenuItem("red singlet"); //Красный
          MenuItem bluef = new MenuItem("blue singlet");//Синий
          
          //вывести пункты продублируя в два вида борьбы
          grw.getItems().addAll(reds,blues);
          fsw.getItems().addAll(redf,bluef);
          
         
         //установить обработчик событий действия для
         //пунктов меню 
         grw.setOnAction(MEHandler);
         fsw.setOnAction(MEHandler);
         gw.setOnAction(MEHandler);
         reds.setOnAction(MEHandler);
         blues.setOnAction(MEHandler);
         redf.setOnAction(MEHandler);
         bluef.setOnAction(MEHandler);
         
         //ввести меню Rurik Wrestling в строку меню
         mb.getMenus().add(menuWrestling);
     }
     
     
     
     //создать меню Help
     void makeHelpMenu(){
         //создать представление типа ImageView для изображения
        // ImageView aboutIV;
       // aboutIV = new ImageView("AboutIcon.gif");
         
         //создать меню help
         Menu helpMenu = new Menu("Help"); //Справка
         
         //создать пункт About и ввести его в меню Help
         MenuItem about = new MenuItem("About");//, aboutIV); //О программе
         helpMenu.getItems().add(about);
         
         //установить обработчик событий действия для
         //пункта About меню Help
         about.setOnAction(MEHandler);
         
         //ввести меню help в строку меню
         mb.getMenus().add(helpMenu);
     }
     
     // создать пункты контекстного меню
    void makeContextMenu(){
        //создать пункты команд редактирования
        //из контекстного меню
        
        MenuItem cut = new MenuItem("Cut"); //Вырезать
        MenuItem copy = new MenuItem("Copy"); //Копировать
        MenuItem paste = new MenuItem("Paste"); //Вставить
        
        //создать контекстное меню (т.е. всплывающее) меню
        //с пунктами для выбора команд редакти
        cut.setOnAction(MEHandler);
        copy.setOnAction(MEHandler);
        paste.setOnAction(MEHandler);
               
    }
    
    //создать панель инструментов
   void makeToolBar(){
       //создать кнопки для панели инструментов
       Button btnSet = new Button("Set Breakpoint");
       Button btnClear = new Button("Clear Breakpoint");
       Button btnResume = new Button("Resume Breakpoint");
       
       //отключить текстовые надписи для кнопок (не используется)
       //  btnSet.setContentDisplay(ContentDisplay.GRAPHIC_ONLY); 
       //  btnClear.setContentDisplay(ContentDisplay.GRAPHIC_ONLY); 
       //  btnResume.setContentDisplay(ContentDisplay.GRAPHIC_ONLY); 
       
       //задать всплывающие подсказки для кнопок
       //Установить точку прерывания
       btnSet.setTooltip(new Tooltip("Set a breakpoint."));
       
       //Отчистить точку прерывания
       btnClear.setTooltip(new Tooltip("Clear a breakpoint"));
       
       //Возобновить выполнение
       btnResume.setTooltip(new Tooltip("Resume execution"));
       
       //создать панель инструментов
       tbDebug = new ToolBar(btnSet, btnClear, btnResume);
       
       //создать обработчик событий от кнопок на панели инструментов
       EventHandler<ActionEvent> btnHandler = new EventHandler<ActionEvent>() {  
      public void handle(ActionEvent ae) {  
        response.setText(((Button)ae.getTarget()).getText());  
      }  
    };  
     
       //установить обработчики событий действия для кнопок
       //на панели инструментов
       btnSet.setOnAction(btnHandler); 
       btnClear.setOnAction(btnHandler); 
       btnResume.setOnAction(btnHandler); 
   }
   
    
}
