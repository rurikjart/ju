/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iftest2;

/**
 *
 * @author artyuhovyv
 */
public class IFTest2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        DynStack mystack1 = new DynStack(5);
        DynStack mystack2 = new DynStack(8);
        
        for(int i=0;i<12;i++) mystack1.push(i);
        for(int i=0;i<20;i++) mystack1.push(i);
        
        
            System.out.println("Stack in mystack1:");
            for(int i=0; i<12; i++) 
            System.out.println(mystack1.pop());

            System.out.println("Stack in mystack2:");
            for(int i=0; i<20; i++) 
            System.out.println(mystack2.pop());
    }
    
}
