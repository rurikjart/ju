/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package breakloop2;

/**
 *
 * @author rurik
 */
public class BreakLoop2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int i = 0;
    
            while(i < 100) {
              if(i == 10) break; // terminate loop if i is 10
              System.out.println("i: " + i);
              i++;
            }
            System.out.println("Loop complete.");
    }
    
}
