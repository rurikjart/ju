/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversion;

/**
 *
 * @author rurik
 */
public class Conversion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
            byte b;
            int i = 257;
            double d = 323.142;

            System.out.println("\nConversion of int to byte.");
            b = (byte) i;
            System.out.println("i and b " + i + " " + b);

            System.out.println("\nConversion of double to int.");
            i = (int) d;
            System.out.println("d and i " + d + " " + i);

            System.out.println("\nConversion of double to byte.");
            b = (byte) d;
            System.out.println("d and b " + d + " " + b);
    }
    
}
