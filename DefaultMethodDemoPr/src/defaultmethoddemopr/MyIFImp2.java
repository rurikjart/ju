/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package defaultmethoddemopr;

/**
 *
 * @author artyuhovyv
 */
public class MyIFImp2 {
     // Here, implementations for both getNumber( ) and getString( ) are provided. 
  public int getNumber() { 
    return 100; 
  } 
 
  public String getString() { 
    return "This is a different string."; 
  }   
}
