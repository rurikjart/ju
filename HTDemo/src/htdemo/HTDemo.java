/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package htdemo;

/**
 *
 * @author artyuhovyv
 */
// Demonstrate a Hashtable. 
import java.util.*; 

public class HTDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Hashtable<String, Double> balance = 
      new Hashtable<String, Double>(); 
 
        Enumeration<String> names; 
        String str; 
        double bal; 

        balance.put("John Doe", 3434.34); 
        balance.put("Tom Smith", 123.22); 
        balance.put("Jane Baker", 1378.00); 
        balance.put("Tod Hall", 99.22); 
        balance.put("Ralph Smith", -19.08); 

        // Show all balances in hashtable. 
        names = balance.keys(); 
        while(names.hasMoreElements()) { 
          str = names.nextElement(); 
          System.out.println(str + ": " + 
                             balance.get(str)); 
        } 

        System.out.println(); 

        // Deposit 1,000 into John Doe's account. 
        bal = balance.get("John Doe"); 
        balance.put("John Doe", bal+1000); 
        System.out.println("John Doe's new balance: " + 
                           balance.get("John Doe")); 
    }
    
}
