/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formatdemo4;

// Demonstrate a field-width specifier. 
import java.util.*; 

/**
 *
 * @author artyuhovyv
 */
public class FormatDemo4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
          Formatter fmt = new Formatter(); 
 
            fmt.format("|%f|%n|%12f|%n|%012f|", 
                       10.12345, 10.12345, 10.12345); 

            System.out.println(fmt); 

            fmt.close(); 
    }
    
}
