/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package explicitchannelread;

import java.io.*; 
import java.nio.*; 
import java.nio.channels.*; 
import java.nio.file.*; 



/**
 *
 * @author artyuhovyv
 */
public class ExplicitChannelRead {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int count;
        Path filepath = null;
        
       // Сначала получить путь к файлу
       
       try {
           filepath = Paths.get("test.txt");
               
       } catch(InvalidPathException e){
           System.err.println("Path Error " + e);
           return;
       }
       
         // Next, obtain a channel to that file within a try-with-resources block. 
    try ( SeekableByteChannel fChan = Files.newByteChannel(filepath) ) 
    { 
 
      // Allocate a buffer. 
      ByteBuffer mBuf = ByteBuffer.allocate(128); 
 
      do { 
        // Read a buffer. 
        count = fChan.read(mBuf); 
 
        // Stop when end of file is reached. 
        if(count != -1) { 
         
          // Rewind the buffer so that it can be read. 
          mBuf.rewind(); 
 
          // Read bytes from the buffer and show 
          // them on the screen as characters. 
          for(int i=0; i < count; i++) 
            System.out.print((char)mBuf.get()); 
        } 
      } while(count != -1); 
 
      System.out.println(); 
    } catch (IOException e) { 
      System.out.println("I/O Error " + e); 
    } 
        
    }
    
}
