/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package defaultmethoddemo;

/**
 *
 * @author artyuhovyv
 */


public class DefaultMethodDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
          MyIFImp obj = new MyIFImp(); 
 
            // Can call getNumber(), because it is explicitly 
            // implemented by MyIFImp: 
            System.out.println(obj.getNumber()); 

            // Can also call getString(), because of default 
            // implementation: 
            System.out.println(obj.getString()); 
    }
    
}
