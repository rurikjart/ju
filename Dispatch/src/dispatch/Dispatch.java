/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dispatch;

/**
 *
 * @author rurik
 */

 // Dynamic Method Dispatch
class A {
   void callme() {
     System.out.println("Inside A's callme method");
  }
}

class B extends A {
  // override callme()
  void callme() {
    System.out.println("Inside B's callme method");
  }
}

class C extends A {
  // override callme()
  void callme() {
    System.out.println("Inside C's callme method");
  }
}

public class Dispatch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
            A a = new A(); // object of type A
            B b = new B(); // object of type B
            C c = new C(); // object of type C
            A r; // obtain a reference of type A    

            r = a; // r refers to an A object
            r.callme(); // calls A's version of callme

            r = b; // r refers to a B object
            r.callme(); // calls B's version of callme

            r = c; // r refers to a C object
            r.callme(); // calls C's version of callme
    }
    
}
