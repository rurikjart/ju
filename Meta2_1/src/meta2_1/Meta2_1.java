/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package meta2_1;

/**
 *
 * @author artyuhovyv
 */
// Show all annotations for a class and a method. 
import java.lang.annotation.*; 
import java.lang.reflect.*; 
 
@Retention(RetentionPolicy.RUNTIME)  
@interface MyAnno { 
  String str(); 
  int val(); 
} 
 
@Retention(RetentionPolicy.RUNTIME)  
@interface What { 
  String description(); 
} 
 
@What(description = "An annotation test class") 
@MyAnno(str = "Meta2_1", val = 99) 
class Meta2_1 { 
 
  @What(description = "An annotation test method") 
  @MyAnno(str = "Testing", val = 100) 
  public static void myMeth() { 
    Meta2_1 ob = new Meta2_1(); 
 
    try { 
      Annotation annos[] = ob.getClass().getAnnotations(); 
 
      // Display all annotations for Meta2_1. 
      System.out.println("All annotations for Meta2_1:"); 
      for(Annotation a : annos) 
        System.out.println(a); 
 
      System.out.println(); 
 
      // Display all annotations for myMeth. 
      Method m = ob.getClass( ).getMethod("myMeth"); 
      annos = m.getAnnotations();  
 
      System.out.println("All annotations for myMeth:"); 
      for(Annotation a : annos) 
        System.out.println(a); 
 
    } catch (NoSuchMethodException exc) { 
       System.out.println("Method Not Found."); 
    } 
  } 
 
  public static void main(String args[]) { 
    myMeth(); 
  } 
}
