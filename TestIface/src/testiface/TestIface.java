/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testiface;

/**
 *
 * @author artyuhovyv
 */
public class TestIface {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Callback c1 = new Client();
        
        c1.callback(42);
        
        Clients c2 = new Clients();
        
        c1 = c2;
        
        c1.callback(107);
    }
    
}
