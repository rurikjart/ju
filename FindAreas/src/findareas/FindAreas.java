/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package findareas;

/**
 *
 * @author rurik
 */
class Figure {
    double dim1;
    double dim2;


        Figure(double a, double b){
            dim1 = a;
            dim2 = b;
        }
        
        double area(){
            double ret;
            System.out.println("Area for Figure is undefined.");
            ret = 0;
           
           return ret;
        }
}

class Rectangle extends Figure {
  Rectangle(double a, double b) {
    super(a, b);
  }

  // override area for rectangle
  double area() {
    System.out.println("Inside Area for Rectangle.");
    return dim1 * dim2;
  }
}

class Triangle extends Figure {

    public Triangle(double a, double b) {
        super(a, b);
    }
    
    double area() {
        System.out.println("Inside Area for Triangle.");
        return dim1 * dim2 / 2;
    }
}

public class FindAreas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Figure f = new Figure(10, 10);
         Rectangle r = new Rectangle(9, 5);
         Triangle t = new Triangle(10, 8);
    
    Figure figref;

    figref = r;
    System.out.println("Area is " + figref.area());
    
    figref = t;
    System.out.println("Area is " + figref.area());
    
    figref = f;
    System.out.println("Area is " + figref.area());
    }
    
}
