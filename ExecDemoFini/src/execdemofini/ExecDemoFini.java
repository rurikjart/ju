/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Wait until notepad is terminated.

package execdemofini;

/**
 *
 * @author artyuhovyv
 */
public class ExecDemoFini {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
            Runtime r = Runtime.getRuntime();
            Process p = null;

            try {
              p = r.exec("notepad");
              p.waitFor();
            } catch (Exception e) {
              System.out.println("Error executing notepad.");
            }
            System.out.println("Notepad returned " + p.exitValue());
    }
    
}
