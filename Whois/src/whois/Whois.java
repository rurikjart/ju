/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whois;


// Demonstrate Sockets. 
import java.net.*; 
import java.io.*; 

/**
 *
 * @author artyuhovyv
 */
public class Whois {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        int c;
        
        // создать сокетное соединение с вэб-сайтом internic.net
        // через порт 43
        
        Socket s = new Socket("whois.internic.net", 43);
        
        //получить потоки ввода-вывода
        InputStream in = s.getInputStream();
        OutputStream out = s.getOutputStream();
        
        //сформировать строку запроса
        String str = (args.length == 0 ? "MHProfessional.com" : args[0]) + "\n";
        
        //преобразовать строку в байты
        byte buf[] = str.getBytes();
        
        //постать запрос
        out.write(buf);
        
        //прочитать ответ 
        while ((c = in.read()) != -1) {            
            System.out.println((char)c);
        }
        
        s.close();
        
       
    }
    
}
