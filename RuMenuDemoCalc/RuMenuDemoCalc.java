// ������ RuMenuDemoCalc ��������

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

/*
<applet code="RuMenuDemoCalc" width=300 height=100>
</applet>
*/

/*�������� ������������ ����, � ������� ����� 
���� ������� ���������������� ���������*/
class RuMenuDemoCalcFrame extends Frame{
    
    String msg = "";
    
    //����������� ������
     RuMenuDemoCalcFrame(String title){
        super(title);
        
        //������� ������ ���� � ������ ��� � ����������� ����
        MenuBar mbar = new MenuBar();
         setMenuBar(mbar);
         
         // �������  ���� File � ������
         Menu fileMenu = new Menu("File");
         //������� ������ ���� ����
         MenuItem openItem, closeItem, sep01Item, exitItem;
         fileMenu.add(openItem = new MenuItem("Open"));
         fileMenu.add(closeItem = new MenuItem("Close"));
         fileMenu.add(sep01Item = new MenuItem("-"));
         fileMenu.add(exitItem = new MenuItem("Exit"));
         //��������� ���� ���� � ������ ����
         mbar.add(fileMenu);
         
         //View
         Menu viewMenu = new Menu("View");
         MenuItem viewOneMenuItem, viewTwoMenuItem;
         viewMenu.add(viewOneMenuItem = new MenuItem("View One"));
         viewMenu.add(viewTwoMenuItem = new MenuItem("View Two"));
         mbar.add(viewMenu);
         
         //Options
         Menu optionsMenu = new Menu("Options");
         MenuItem settingMenuItem, graphicsMenuItem, soundMenuItem;
         optionsMenu.add(settingMenuItem = new MenuItem("Setting"));
         optionsMenu.add(graphicsMenuItem = new MenuItem("Graphics"));
         optionsMenu.add(soundMenuItem = new MenuItem("Sound"));
         
         //������� ��� Options ������� main
         Menu subMenuMain = new Menu("main");
         MenuItem secMenuItem, mintMenuItem;
         subMenuMain.add(secMenuItem = new MenuItem("sec"));
         subMenuMain.add(mintMenuItem = new MenuItem("mint"));
         
         //���������� ������� main � ���� Options
         optionsMenu.add(subMenuMain);
         
           //��������� ���� Options � ������ ����
         mbar.add(optionsMenu);
         
         //Help
         Menu helpMenu = new Menu("Help");
         MenuItem versionPcMenuItem;
         helpMenu.add(versionPcMenuItem = new MenuItem("Vercion PC"));
         mbar.add(helpMenu);
         
       
         
         
         //������� ������ ��� ��������� ������� �������� � ������� �� ���������
         RuMenuDemoCalcHandler handler = new RuMenuDemoCalcHandler(this);
         //���������������� ���� ������ ��� ������ �������
         //�������� � ������� �� ���������
         openItem.addActionListener(handler);
         closeItem.addActionListener(handler);
         exitItem.addActionListener(handler);
         viewOneMenuItem.addActionListener(handler);
         viewTwoMenuItem.addActionListener(handler);
         settingMenuItem.addActionListener(handler);
         graphicsMenuItem.addActionListener(handler);
         soundMenuItem.addActionListener(handler);
         secMenuItem.addActionListener(handler);
         mintMenuItem.addActionListener(handler);
        versionPcMenuItem.addActionListener(handler);
        
       
         
         
        
        // ������� ������ ��� ��������� ������� � ����
       RuMenuDemoCalcWindowAdapter ruMenuDemoCalcWindowAdapter = new RuMenuDemoCalcWindowAdapter(this);
       //���������������� ���� ������ � �������� ���������
       //������� � ����
         addWindowListener(ruMenuDemoCalcWindowAdapter);
      
    }
     
     public void paint(Graphics g){
         g.drawString(msg, 10, 200);
     }
     
     
}    
     
     
     class RuMenuDemoCalcHandler implements ActionListener, ItemListener {
        
         RuMenuDemoCalcFrame ruMenuDemoCalcFrame;
        
         //�����������
         public RuMenuDemoCalcHandler (RuMenuDemoCalcFrame ruMenuDemoCalcFrame){
             
         this.ruMenuDemoCalcFrame = ruMenuDemoCalcFrame; 
         
        }
         
         
        //������������ ������� ��������  
        @Override
        public void actionPerformed(ActionEvent ae) {
            String msg = "You selected "; //������ ����� ����
            String arg = ae.getActionCommand();
            if(arg.equals("Open"))
                msg+="Open.";
            else if(arg.equals("Close"))
                msg+="Close.";
            else if(arg.equals("Exit"))
                 msg+="Exit.";
            else if(arg.equals("View One"))
                msg+="View One.";
            else if(arg.equals("View Two"))
                msg+="View Two.";
            else if(arg.equals("Setting"))
                msg+="Setting.";
            else if(arg.equals("Graphics"))
                msg+="Graphics.";
            else if(arg.equals("Sound"))
                msg+="Sound.";
            else if(arg.equals("sec"))
                msg+="sec.";
            else if(arg.equals("mint"))
                msg+="mint.";
            else if(arg.equals("Vercion PC"))
                msg+="Vercion PC.";
            ruMenuDemoCalcFrame.msg = msg;
            ruMenuDemoCalcFrame.repaint();
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            
            ruMenuDemoCalcFrame.repaint();
        }
         
     }
     
    



//����� ������� ��� ���������� ��������� �� ����-������
 class RuMenuDemoCalcWindowAdapter extends WindowAdapter{
    
    RuMenuDemoCalcFrame ruMenuDemoCalcFrame;
    
    public  RuMenuDemoCalcWindowAdapter(RuMenuDemoCalcFrame ruMenuDemoCalcFrame){
        this.ruMenuDemoCalcFrame = ruMenuDemoCalcFrame;
        
    }
    
    @Override
    public void windowClosing(WindowEvent we){
        ruMenuDemoCalcFrame.setVisible(false);
    }
    
    
}


public class RuMenuDemoCalc extends Applet {
    Frame f; // Frame �������� ������������� ������ RuMenuDemoCalcFrame
    //���� ����� ���������� ������
    
    public void init(){
        //�������������
        
          //������� ���� �����, ����� � ������ 
         Label sringOne = new Label("String One", Label.RIGHT);
         Label stringTwo = new Label("String Two", Label.RIGHT);
         
         TextField strFieldOne, strFieldTwo;
         
         strFieldOne = new TextField(10);
         strFieldTwo = new TextField(10);
         
         add(sringOne);
         add(strFieldOne);
         add(stringTwo);
         add(strFieldTwo);
         
          // ���������������� ���� ��� ��������� ������� ��������
        //strFieldOne.addActionListener(this);
       // strFieldTwo.addActionListener(this);
        
        
        setBackground(Color.BLUE);
        
        f = new RuMenuDemoCalcFrame("A Frame Window");
        
        int width = Integer.parseInt(getParameter("width"));
        int height = Integer.parseInt(getParameter("height"));
        
        setSize(new Dimension(width, height));
                      
        f.setSize(width, height);
        f.setVisible(true);
        
    }
    
    /*����� ���������� ������ ����� ������ init().
    ���������� ���-�� ����� ����������� ������. */ 
   
    public void start(){
        //������ ��� ����������� ���������� ������
        f.setVisible(true);
        
    }
    
    // ���� ����� ���������� ��� ��������� ������
  
    public void stop(){
        // ������������� ���������� ������
        f.setVisible(false);
        
    }
    
    /* ���� ����� ���������� ����� ������������ ������. 
    ��� ��������� ����������� ����� */
    
    public void destroy(){
        //��������� ���������� ��������
    }
    
       /* ���� ����� ����������, ����� ���� ������
    ������ ���� ��������������. */
    
    
    public void paint(Graphics g){
        // �������� ������������� ���������� ����
      
        showStatus("��� ���� ������ �������������� � ������������...");
    }
    
}