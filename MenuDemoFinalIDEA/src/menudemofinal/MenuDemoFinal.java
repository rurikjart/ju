/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menudemofinal;

// продемонстрировать меню - окончательный вариант

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.input.*;
import javafx.scene.image.*;
import javafx.beans.value.*;
import jdk.nashorn.internal.ir.CallNode;


/**
 *
 * @author artyuhovyv
 */

public class MenuDemoFinal extends Application {

    MenuBar mb;
    EventHandler<ActionEvent> MEHandler;
    ContextMenu editMenu;
    ToolBar tbDebug;
    
    Label response;
    
    public static void main(String[] args) {
       // запустить JavaFX-приложение, вызвав метод Launch()
        launch(args);
    }
    
    //переопределить метод start()
    @Override
    public void start(Stage myStage) {
        
        //Присвоить заголовок подмосткам
        myStage.setTitle("Demonstrate Menus -- Final Version");
        //Демонстрация меню - окончательный вариант
        
        //Использовать панель граничной компоновки BorderPane
        //в качестве корневого узла
        final BorderPane rootNode = new BorderPane();
        
        //создать сцену
        Scene myScene = new Scene(rootNode,300,300);
        
        //установить сцену на подмостках
        myStage.setScene(myScene);
        
        //зоздать метку для отображения результатов выбора
        //из разных элементов управления ГПИ приложения
        response = new Label();
        
        //создать один приемник действий для обработки
        //всех событий действий, наступающих в меню
        
        MEHandler = new EventHandler<ActionEvent>() { 
      public void handle(ActionEvent ae) {  
        String name = ((MenuItem)ae.getTarget()).getText(); 
 
        if(name.equals("Exit")) Platform.exit(); 
 
        response.setText( name + " selected"); 
         }  
       }; 
        
        //создать строку меню
        mb = new MenuBar();
        
        //создать меню File
        makeFileMenu();
        
        //создать меню Options
        makeOptionsMenu();
        
        //создать меню Help
        makeHelpMenu();
        
        //вывести строку меню в верхней области панели
        rootNode.setTop(mb);
        
        
        myStage.show();
    }
    
    //создать меню File с мнемоникой
    void makeFileMenu(){
        Menu fileMenu = new Menu("_File");//Файл
        
        //создать отдельные пункты меню File
        MenuItem open = new MenuItem("Open");//Открыть
        MenuItem close = new MenuItem("Close");//Закрыть
        MenuItem save = new MenuItem("Save");//Сохранить
        MenuItem exit = new MenuItem("Exit");//Выход
        
        //вывести пункты в меню File
        fileMenu.getItems().addAll(open, close, save, new SeparatorMenuItem(), exit);
        
        // вывести оперативные клавиши для быстрого выбора
        // пунктов из меню File
        open.setAccelerator(KeyCombination.keyCombination("shortcut+O"));
        close.setAccelerator(KeyCombination.keyCombination("shortcut+C"));
        save.setAccelerator(KeyCombination.keyCombination("shortcut+S"));
        exit.setAccelerator(KeyCombination.keyCombination("shortcut+E"));
        
        //установить обработчики событий действия для пунктов меню File
        open.setOnAction(MEHandler);
        close.setOnAction(MEHandler);
        save.setOnAction(MEHandler);
        exit.setOnAction(MEHandler);
        
        //ввести меню File в строку меню
        mb.getMenus().add(fileMenu);
    }

    //сщздать меню Options
     void makeOptionsMenu() {
       
         
         Menu optionsMenu = new Menu("Options");//Параметры
         
         //создать подменю Colors
         Menu colorsMenu = new Menu("Colors");//Цвета
      
         
         //использовать отмечаемые флажками пункты меню,
         // чтобы пользователь мог выбрать сразу несколько цветов
         CheckMenuItem red = new CheckMenuItem("red"); //Красный
         CheckMenuItem green = new CheckMenuItem("Green");//Зеленый
         CheckMenuItem blue = new CheckMenuItem("Blue");//Синий
         
         //ввестиотмечаемые флажками пункты в подменю Colors,
         // а само подменю Colors - в меню Options
         
         colorsMenu.getItems().addAll(red,green,blue);
         optionsMenu.getItems().add(colorsMenu);
         
         //задать зеленый цвет в качестве исходного выбираемого
         green.setSelected(true);
         
          //создать подменю Priority
         Menu priorityMenu = new Menu("Priority");//Приоритет
         
         //использовать отмечаемые кнопками-переключателями пункты
         //меню для установки приоритета. Благодаря этому в меню не
         //только отображается установленный приоритет, но и 
         //гарантируется установка одного и только одного приоритета
         RadioMenuItem high = new RadioMenuItem("High");
         RadioMenuItem low = new RadioMenuItem("Low");
         
         //Создать группу кнопок-переключателей в пунктах подменю Priority
         ToggleGroup tg = new ToggleGroup();
         high.setToggleGroup(tg);
         low.setToggleGroup(tg);
         
         //отметить пункт меню High как исходно выбираемый
         high.setSelected(true);
         
         //ввести отмечаемые кнопками-переключателями пункты
         //в подменю Priority, последнее - в меню Options
         priorityMenu.getItems().addAll(high,low);
         optionsMenu.getItems().add(priorityMenu);
         
         //ввести разделитель
         optionsMenu.getItems().add(new SeparatorMenuItem());
         
         //создать пункт меню Reset и ввести его в меню Options
         MenuItem reset = new MenuItem("Reset"); //Сбросить
         optionsMenu.getItems().add(reset);
         
         //установить обработчики событий действия для
         //пунктов меню Options
         red.setOnAction(MEHandler);
         green.setOnAction(MEHandler);
         blue.setOnAction(MEHandler);
         high.setOnAction(MEHandler);
         low.setOnAction(MEHandler);
         reset.setOnAction(MEHandler);
         
         //использовать приемник событий изменения, чтобы оперативно
         //реагировать на изменения в отметке пунктов подменю Priority
         //кнопками-переключателями
         tg.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
         public void changed(ObservableValue<? extends Toggle> changed, Toggle oldVal,Toggle newVal){
         if(newVal==null) return;
         
         //привести значение newVal к типу RadioButton
         RadioMenuItem rmi = (RadioMenuItem) newVal;
         
         //отобразить результат выбора приоритета
         response.setText("Prioruty selected is " + rmi.getText());
         //выбран указанный приоритет
         
         }
         });
         
          // вывести меню Оptions в строку меню
          mb.getMenus().add(optionsMenu);
         
    }

     //создать меню Help
     void makeHelpMenu(){
         //создать представление типа ImageView для изображения
         ImageView aboutIV;
       // aboutIV = new ImageView("AboutIcon.gif");
         
         //создать меню help
         Menu helpMenu = new Menu("Help"); //Справка
         
         //создать пункт About и ввести его в меню Help
         MenuItem about = new MenuItem("About");//, aboutIV); //О программе
         helpMenu.getItems().add(about);
         
         //установить обработчик событий действия для
         //пункта About меню Help
         about.setOnAction(MEHandler);
         
         //ввести меню help в строку меню
         mb.getMenus().add(helpMenu);
     }
   
    
}
