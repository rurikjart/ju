/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxlabeldemo;

import javafx.application.*; 
import javafx.scene.*; 
import javafx.stage.*; 
import javafx.scene.layout.*; 
import javafx.scene.control.*; 

/**
 *
 * @author artyuhovyv
 */
public class JavaFXLabelDemo extends Application {
    
    @Override
   // Override the start() method. 
  public void start(Stage myStage) { 
 
    // Give the stage a title. 
    myStage.setTitle("Demonstrate a JavaFX label."); 
 
    // Use a FlowPane for the root node. 
    FlowPane rootNode = new FlowPane(); 
 
    // Create a scene. 
    Scene myScene = new Scene(rootNode, 300, 200); 
 
    // Set the scene on the stage. 
    myStage.setScene(myScene); 
 
    // Create a label. 
    Label myLabel = new Label("This is a JavaFX label"); 
 
    // Add the label to the scene graph. 
    rootNode.getChildren().add(myLabel); 
 
    // Show the stage and its scene. 
    myStage.show(); 
  } 

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
