/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraysdemo;

/**
 *
 * @author artyuhovyv
 */

// Demonstrate Arrays  
import java.util.*;  

public class ArraysDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
                // Allocate and initialize array. 
          int array[] = new int[10];  
          for(int i = 0; i < 10; i++)  
            array[i] = -3 * i;  

          // Display, sort, and display the array. 
          System.out.print("Original contents: ");  
          display(array);  
          Arrays.sort(array);  
          System.out.print("Sorted: ");  
          display(array);  

          // Fill and display the array. 
          Arrays.fill(array, 2, 6, -1);  
          System.out.print("After fill(): ");  
          display(array);  

          // Sort and display the array. 
          Arrays.sort(array);  
          System.out.print("After sorting again: ");  
          display(array);  

          // Binary search for -9. 
          System.out.print("The value -9 is at location ");  
          int index =   
            Arrays.binarySearch(array, -9);  

          System.out.println(index);  
        
    }
    
    static void display(int array[]) {  
    for(int i: array) 
      System.out.print(i + " ");  
 
    System.out.println();  
  }  
    
}
