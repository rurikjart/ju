/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgbreak;

/**
 *
 * @author rurik
 */
public class Break {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
           boolean t = true;

            first: {
              second: {
                third: {
                  System.out.println("Before the break.");
                  if(t) break second; // break out of second block
                  System.out.println("This won't execute");
                }
                System.out.println("This won't execute");
              }
              System.out.println("This is after second block.");
            }
    }
    
}
