// This applet does NOT use an inner class.
import java.applet.*;
import java.awt.event.*;
/*
  <applet code="InnerClassDemo" width=200 height=100>
  </applet>
 
*/
 
public class InnerClassDemo extends Applet {
  public void init() {
    addMouseListener(new MyMouseAdapter());
  }
  
  
  //inner class
  
		  class MyMouseAdapter extends MouseAdapter {

				  public void mousePressed(MouseEvent me) {
					showStatus("Mouse Pressed.");
				  }	
		  
		}
  
}

