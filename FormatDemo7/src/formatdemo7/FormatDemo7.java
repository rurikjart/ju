/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formatdemo7;


// Use automatic resource management with Formatter.
import java.util.*; 

/**
 *
 * @author artyuhovyv
 */
public class FormatDemo7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
          try (Formatter fmt = new Formatter())
    {
      fmt.format("Formatting %s is easy %d %f", "with Java", 10, 98.6); 
      System.out.println(fmt); 
    }
        
   }
    
}
