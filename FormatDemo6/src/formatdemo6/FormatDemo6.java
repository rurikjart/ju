/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formatdemo6;

// Use arguments indexes to simplify the 
// creation of a custom time and date format. 
import java.util.*; 

/**
 *
 * @author artyuhovyv
 */
public class FormatDemo6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Formatter fmt = new Formatter(); 
            Calendar cal = Calendar.getInstance(); 

            fmt.format("Today is day %te of %<tB, %<tY", cal); 
            System.out.println(fmt); 
            fmt.close();
    
    }
    
}
