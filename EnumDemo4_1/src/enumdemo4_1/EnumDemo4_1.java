/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumdemo4_1;

enum Apple {  
  Jonathan, GoldenDel, RedDel, Winesap, Cortland 
} 

/**
 *
 * @author artyuhovyv
 */
public class EnumDemo4_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         Apple ap, ap2, ap3; 
 
    // Obtain all ordinal values using ordinal(). 
    System.out.println("Here are all apple constants" + 
                       " and their ordinal values: "); 
    for(Apple a : Apple.values()) 
      System.out.println(a + " " + a.ordinal()); 
 
    ap =  Apple.RedDel; 
    ap2 = Apple.GoldenDel; 
    ap3 = Apple.RedDel; 
 
    System.out.println(); 
 
    // Demonstrate compareTo() and equals() 
    if(ap.compareTo(ap2) < 0) 
      System.out.println(ap + " comes before " + ap2); 
 
    if(ap.compareTo(ap2) > 0) 
      System.out.println(ap2 + " comes before " + ap); 
 
    if(ap.compareTo(ap3) == 0) 
      System.out.println(ap + " equals " + ap3); 
   
    System.out.println(); 
 
    if(ap.equals(ap2)) 
      System.out.println("Error!"); 
 
    if(ap.equals(ap3)) 
      System.out.println(ap + " equals " + ap3); 
 
    if(ap == ap3) 
      System.out.println(ap + " == " + ap3); 
 
    }
    
}
