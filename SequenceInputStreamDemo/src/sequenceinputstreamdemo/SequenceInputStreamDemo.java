/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sequenceinputstreamdemo;


// Demonstrate sequenced input. 
// This program uses the traditional approach to closing a file. 
 
import java.io.*; 
import java.util.*; 
 

/**
 *
 * @author artyuhovyv
 */

class InputStreamEnumerator implements Enumeration<FileInputStream> { 
  private Enumeration<String> files; 
 
  public InputStreamEnumerator(Vector<String> files) { 
    this.files = files.elements(); 
  } 
 
  public boolean hasMoreElements() { 
    return files.hasMoreElements(); 
  } 
 
  public FileInputStream nextElement() { 
    try { 
      return new FileInputStream(files.nextElement().toString()); 
    } catch (IOException e) { 
      return null; 
    } 
  } 
} 

 class SequenceInputStreamDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int c; 
        Vector<String> files = new Vector<String>(); 

        files.addElement("file1.txt"); 
        files.addElement("file2.txt"); 
        files.addElement("file3.txt"); 
        InputStreamEnumerator ise = new InputStreamEnumerator(files); 
        InputStream input = new SequenceInputStream(ise); 

        try { 
          while ((c = input.read()) != -1) 
            System.out.print((char) c); 
        } catch(NullPointerException e) { 
          System.out.println("Error Opening File."); 
        } catch(IOException e) { 
          System.out.println("I/O Error: " + e); 
        } finally { 
          try { 
            input.close(); 
          } catch(IOException e) { 
            System.out.println("Error Closing SequenceInputStream"); 
          } 
        } 
    }
    
}
