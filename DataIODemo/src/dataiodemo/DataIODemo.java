/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataiodemo;

// Demonstrate DataInputStream and DataOutputStream. 
// This program uses try-with-resources. It requires JDK 7 or later. 
 
import java.io.*; 

/**
 *
 * @author artyuhovyv
 */
public class DataIODemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        // First, write the data.  
        try ( DataOutputStream dout = 
                new DataOutputStream(new FileOutputStream("Test.dat")) ) 
        { 
          dout.writeDouble(98.6); 
          dout.writeInt(1000); 
          dout.writeBoolean(true); 

        } catch(FileNotFoundException e) { 
          System.out.println("Cannot Open Output File"); 
          return; 
        } catch(IOException e) { 
          System.out.println("I/O Error: " + e); 
        }  

        // Now, read the data back. 
        try ( DataInputStream din =  
                new DataInputStream(new FileInputStream("Test.dat")) ) 
        { 

          double d = din.readDouble(); 
          int i = din.readInt(); 
          boolean b = din.readBoolean(); 

          System.out.println("Here are the values: " + 
                              d + " " + i + " " + b); 
        } catch(FileNotFoundException e) { 
          System.out.println("Cannot Open Input File"); 
          return; 
        } catch(IOException e) { 
          System.out.println("I/O Error: " + e); 
        } 
    }
    
}
