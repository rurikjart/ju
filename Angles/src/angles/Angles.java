/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package angles;

// Demonstrate toDegrees() and toRadians().

/**
 *
 * @author artyuhovyv
 */
public class Angles {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
            double theta = 120.0;

            System.out.println(theta + " degrees is " +
                               Math.toRadians(theta) + " radians.");

            theta = 1.312;
            System.out.println(theta + " radians is " +
                               Math.toDegrees(theta) + " degrees.");
    }
    
}
