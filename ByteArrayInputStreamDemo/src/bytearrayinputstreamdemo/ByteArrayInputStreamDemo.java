/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bytearrayinputstreamdemo;


// Demonstrate ByteArrayInputStream. 
import java.io.*; 

/**
 *
 * @author artyuhovyv
 */
public class ByteArrayInputStreamDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String tmp = "abcdefghijklmnopqrstuvwxyz"; 
        byte b[] = tmp.getBytes(); 

        ByteArrayInputStream input1 = new ByteArrayInputStream(b); 
        ByteArrayInputStream input2 = new ByteArrayInputStream(b,0,3); 
    }
    
}
