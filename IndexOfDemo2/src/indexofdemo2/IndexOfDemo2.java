/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indexofdemo2;

/**
 *
 * @author rurik
 */
public class IndexOfDemo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            StringBuffer sb = new StringBuffer("one two one");
            int i;

            i = sb.indexOf("one");
            System.out.println("First index: " + i);

            i = sb.lastIndexOf("one");
            System.out.println("Last index: " + i);
    }
    
}
