/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package explicitchannelwrite2;

// Write to a file using NIO. Pre-JDK 7 Version. 
import java.io.*; 
import java.nio.*; 
import java.nio.channels.*; 

/**
 *
 * @author artyuhovyv
 */
public class ExplicitChannelWrite2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
          FileOutputStream fOut = null; 
    FileChannel fChan = null; 
    ByteBuffer mBuf; 
 
    try { 
      // First, open the output file. 
      fOut = new FileOutputStream("test.txt"); 
 
      // Next, get a channel to the output file. 
      fChan = fOut.getChannel(); 
 
      // Create a buffer. 
      mBuf = ByteBuffer.allocate(26); 
 
      // Write some bytes to the buffer. 
      for(int i=0; i<26; i++) 
        mBuf.put((byte)('A' + i)); 
 
      // Rewind the buffer so that it can be written. 
      mBuf.rewind(); 
 
      // Write the buffer to the output file. 
      fChan.write(mBuf); 
    } catch (IOException e) { 
      System.out.println("I/O Error " + e); 
    } finally { 
      try { 
        if(fChan != null) fChan.close(); // close channel 
      } catch(IOException e) { 
        System.out.println("Error Closing Channel."); 
      } 
      try { 
        if(fOut != null) fOut.close(); // close file 
      } catch(IOException e) { 
        System.out.println("Error Closing File."); 
      } 
    } 
  }
    
}
