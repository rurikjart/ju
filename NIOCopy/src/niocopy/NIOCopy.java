/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package niocopy;

// Copy a file using NIO. Requires JDK 7 or later. 
import java.io.*; 
import java.nio.*; 
import java.nio.channels.*; 
import java.nio.file.*; 

/**
 *
 * @author artyuhovyv
 */
public class NIOCopy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         if(args.length != 2) { 
      System.out.println("Usage: Copy from to"); 
      return; 
    } 
 
    try { 
      Path source = Paths.get(args[0]); 
      Path target = Paths.get(args[1]); 
 
      // Copy the file. 
      Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING); 
 
    } catch(InvalidPathException e) { 
      System.out.println("Path Error " + e); 
    } catch (IOException e) { 
      System.out.println("I/O Error " + e); 
    } 
  }
    
}
