/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leftjustify;

import java.util.*;


/**
 *
 * @author artyuhovyv
 */
public class LeftJustify {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Formatter fmt = new Formatter(); 
 
        // Right justify by default 
        fmt.format("|%10.2f|", 123.123); 
        System.out.println(fmt); 
        fmt.close();

        // Now, left justify. 
        fmt = new Formatter(); 
        fmt.format("|%-10.2f|", 123.123); 
        System.out.println(fmt); 
        fmt.close();
    }
    
}
