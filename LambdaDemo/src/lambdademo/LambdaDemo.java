/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdademo;

interface MyNumber {
    double getValue();
}

/**
 *
 * @author artyuhovyv
 */
public class LambdaDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MyNumber myNum;
        
        myNum = () -> 123.45;
        
        System.out.println("A fixed value: " + myNum.getValue());
        
        myNum = () -> Math.random() * 100;
        
        System.out.println("A random value: " + myNum.getValue()); 
        System.out.println("Another random value: " + myNum.getValue()); 
    }
    
}
