/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdademo2;

interface NumericTest {
    boolean test(int n);
}

/**
 *
 * @author artyuhovyv
 */
public class LambdaDemo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NumericTest isEven = (n) -> (n % 2) == 0;
        
        if(isEven.test(10)) System.out.println("10 is even");
        if(!isEven.test(9)) System.out.println("9 is not even");
        
        NumericTest isNonNeg = (n) -> n>=0;
        
        if(isNonNeg.test(1)) System.out.println("1 is non-negative"); 
        if(!isNonNeg.test(-1)) System.out.println("-1 is negative");
    }
    
}
