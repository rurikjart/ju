/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package substringcons;

/**
 *
 * @author artyuhovyv
 */
public class SubStringCons {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        byte ascii[] = {65, 66, 67, 68, 69, 70};
        
        String s1 = new String(ascii);
        System.out.println(s1);
        
        String s2 = new String(ascii, 2, 3);
        System.out.println(s2);        
                
     }
    
}
