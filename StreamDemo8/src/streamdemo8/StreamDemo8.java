/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streamdemo8;

// Use an iterator with a stream. 
 
import java.util.*; 
import java.util.stream.*; 

/**
 *
 * @author artyuhovyv
 */
public class StreamDemo8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
          // Create a list of Strings. 
                ArrayList<String> myList = new ArrayList<>( ); 
                myList.add("Alpha"); 
                myList.add("Beta"); 
                myList.add("Gamma"); 
                myList.add("Delta"); 
                myList.add("Phi"); 
                myList.add("Omega"); 

                // Obtain a Stream to the array list. 
                Stream<String> myStream = myList.stream(); 

                // Obtain an iterator to the stream. 
                Iterator<String> itr = myStream.iterator(); 

                // Iterate the elements in the stream. 
                while(itr.hasNext())  
                  System.out.println(itr.next()); 
    }
    
}
