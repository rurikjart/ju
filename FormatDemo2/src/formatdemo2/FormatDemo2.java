/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formatdemo2;


// Demonstrate the %f and %e format specifiers. 
import java.util.*; 

/**
 *
 * @author artyuhovyv 
 */
public class FormatDemo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Formatter fmt = new Formatter(); 
 
        for(double i=1.23; i < 1.0e+6; i *= 100) { 
          fmt.format("%f %e", i, i); 
          System.out.println(fmt); 
        } 
        fmt.close(); 
    }
    
}
