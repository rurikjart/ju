/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doubledemo;

/**
 *
 * @author artyuhovyv
 */
public class DoubleDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Double d1 = new Double(3.14159);
        Double d2 = new Double("314159E-5");
        
        System.out.println(d1 + " = " + d2 + " -> " + d1.equals(d2));
    }
    
}

