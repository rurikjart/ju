/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tostringdemo;

class Box {
  double width;
  double height;
  double depth;

  Box(double w, double h, double d) {
    width = w;
    height = h;
    depth = d;
  }

  public String toString() {
    return "Dimensions are " + width + " by " + 
            depth + " by " + height + ".";
  }
}

/**
 *
 * @author artyuhovyv
 */

public class ToStringDemo {

    /**
     * @param args the command line arguments
     */
    
    // Override toString() for Box class.

    
    public static void main(String[] args) {
            Box b = new Box(10, 12, 14);
            String s = "Box b: " + b; // concatenate Box object

            System.out.println(b); // convert Box to string
            System.out.println(s);
    }
    
}
