/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package makestring;

/**
 *
 * @author artyuhovyv
 */
public class MakeString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        char c[] = {'J', 'a', 'v', 'a'};
        
        String s1 = new String(c);
        String s2 = new String(s1);
        
        System.out.println(s1);
        System.out.println(s2);
        
    }
    
}
