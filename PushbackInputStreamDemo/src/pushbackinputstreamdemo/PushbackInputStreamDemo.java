/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pushbackinputstreamdemo;


// Demonstrate unread(). 
// This program uses try-with-resources. It requires JDK 7 or later. 
 
import java.io.*; 

/**
 *
 * @author artyuhovyv
 */
public class PushbackInputStreamDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
            String s = "if (a == 4) a = 0;\n"; 
            byte buf[] = s.getBytes(); 
            ByteArrayInputStream in = new ByteArrayInputStream(buf); 
            int c; 

            try ( PushbackInputStream f = new PushbackInputStream(in) ) 
            { 
              while ((c = f.read()) != -1) { 
                switch(c) { 
                case '=': 
                  if ((c = f.read()) == '=') 
                    System.out.print(".eq."); 
                  else { 
                    System.out.print("<-"); 
                    f.unread(c); 
                  } 
                  break; 
                default: 
                  System.out.print((char) c); 
                  break; 
                } 
              } 
            } catch(IOException e) { 
              System.out.println("I/O Error: " + e); 
            } 
    }
    
}
