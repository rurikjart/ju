/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appenddemo;

/**
 *
 * @author rurik
 */
public class AppendDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         String s;
        int a = 42;
        StringBuffer sb = new StringBuffer(40);

        s = sb.append("a = ").append(a).append("!").toString();
        System.out.println(s);
    }
    
}
