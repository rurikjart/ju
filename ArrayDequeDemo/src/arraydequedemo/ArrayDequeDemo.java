/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraydequedemo;

/**
 *
 * @author artyuhovyv
 */

import java.util.*;

public class ArrayDequeDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayDeque<String> adq = new ArrayDeque<String>();

         // Use an ArrayDeque like a stack.
        adq.push("A"); 
        adq.push("B");  
        adq.push("D"); 
        adq.push("E"); 
        adq.push("F"); 

        System.out.print("Popping the stack: ");

        while(adq.peek() != null) 
          System.out.print(adq.pop() + " ");

        System.out.println();
    }
    
}
